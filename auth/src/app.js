const express = require("express")
const app = express()
const {port, host} = require('./configuration')
const {connectDB} = require('./helper/db.js')

function Start(){
    app.listen(port, () => {
        console.log(`AUTH is working on port ${port}`)
        console.log(`HOST      is ${host}`)
    })
}

app.get("/auth", (req, res) =>{
    res.send("AUTH!")
})


connectDB()
    .on('error', console.error.bind(console, "connection error:"))
    .on('disconnected', connectDB)
    .once('open', Start)

app.get("api/user", (req,res)=> {
    res.json({
        id: "1998",
        firstName: "Oleksandr",
        lastName: "Samko"
    })
})

