const mongoose = require('mongoose')
const {db} = require('./../configuration')

module.exports.connectDB = () => {
    mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology: true})
    return mongoose.connection
}
const Smartphone = mongoose.model('Smartphone', { name: String });

const xiaomi1  = new Smartphone({ name: 'Redmi 9' });
const xiaomi2  = new Smartphone({ name: 'Redmi 10' });

xiaomi1.save().then(() => console.log('AUTH is SAVE !'));
