const express = require("express")
const app = express()
const {connectDB} = require("./helpers/db")
const {port, host} = require("./configuration")

app.get("/test", (req, res) => {
    res.send("Server is work")
})

connectDB()
    .on('error', console.error.bind(console, 'connection error:'))
    .once('open', function () {
        // we're connected!
        app.listen(port, () => {
            console.log(`API Service is work on ${port}`)
            console.log(`Host is ${host}`)
        })
    });